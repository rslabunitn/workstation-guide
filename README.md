# RSLab Workstation Guide

In this repository you will find the guidelines and resources needed to use the RSLab shared workstations.
Please carefully read both of the linked pages before beginning to use them.

For any problem, doubt or suggestion write to: massimo.santoni@unitn.it and/or to the Google Space linked below.

## Pages

* [workstations guidelines](https://bitbucket.org/rslabunitn/workstation-guide/src/master/workstation-guidelines.md)
* [technical guide](https://bitbucket.org/rslabunitn/workstation-guide/src/master/technical-guide.md)


## Useful links

* [Logger website](https://rslab.disi.unitn.it/logger/)
* [RSLab GPUs Google space](https://chat.google.com/room/AAAAPnpHXY8?cls=7)
