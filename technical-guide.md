Here you can find all the information and detail about using the RSLab's workstations.

For any problem, doubt or suggestion write to: massimo.santoni@unitn.it

# Accessing the workstation 
First of all you should be able to access the University internal network, by connecting to wifi or ethernet outlets in university buildings or by using the [VPN serice](https://icts.unitn.it/evento/collegarsi-alla-vpn).
Once you received your credentials you could access the workstation using a remote ssh terminal.
Many ssh clients are available, integrated in the OS or standalone. For example on Windows a couple of good options are:

*  [Tabby](https://tabby.sh/)
*  [MobaXTerm](https://mobaxterm.mobatek.net/)
*  [Visual Studio Code](https://code.visualstudio.com/) with many useful plugin (i.e. ssh, docker, interpreter and many others)

# Hardware info
You should have received a workstation name or IP address. With that at the following link you can find the actual status of the workstation and some basic hardware information updated every 10 minutes:
https://rslab.disi.unitn.it/logger/

The `| S |` button allows you to see only one system to allow an easy link to be bookmarked.
On this page you will find the GPU model and driver version if present.

Please note that the system has usually 2 hard drives, a fast system drive and a large mechanical drive (1TB) mounted to `/media/datapart`. The best practice is to keep on the system drive only the data that you are really using to avoid filling it up.

**Regularly check the status of the hard drive space using the `df -h` command or the [logger website](https://rslab.disi.unitn.it/logger/)**

# Software
Most of the workstation has just the basic software plus some utility to help you managing the system (i.e. rclone to pull and push file to Google Drive).
For any request write to the mail in the 1st paragraph.

Any specific software you need should be managed, if possible, through docker containers.

## Docker 
While using a shared system it is not always easy to adapt to the need of all the users without strict limitation on the possible installation or the risk of clogging up the OS with hundreds of packages and libraries. Docker containers are an effective and elegant solution to this problem offering an easy reproducible isolated environment where to run your code.

If you are not familiar with Docker you could start from here: [C. Boettiger - An introduction to Docker for reproducible research](https://arxiv.org/pdf/1410.0846.pdf) 

or from one of the thousand videos, tutorials and articles present online like this one: [Docker tutoral](https://docker-curriculum.com/).
Especially in the case of GPU accelerated Deep Learning application Docker containers are very helpful.

[Here](https://www.tensorflow.org/install/docker#gpu_support) you can find some examples on how to use tensorflow with docker.
 
Since the docker container being isolated cannot "see" the host filesystem you could mount your folder inside the container. I advise you to use the following flags with docker run: (*[...] represent the other parts of the run command*)
     
    docker run [...] -u $(id -u):$(id -g) -v $(pwd):$(pwd) -w $(pwd) [...]
that allows you to:

*  use your user and group `(-u)`
*  mount your home in the container `(-v)` 
*  move the terminal there `(-w)`

You can use `(-v)` also to mount `/media/datapart` or `/media/faststorage` in the container (e.g, `-v /media/datapart/username:/path/inside/container`.
More info about docker volumes [here](https://docs.docker.com/storage/volumes/).

### Lifecycle of a Docker Container
To exploit at most the functionalities of docker containers is good to understand their different components and basic operations: 
[This article](https://medium.com/future-vision/docker-lifecycle-tutorial-and-quickstart-guide-c5fd5b987e0d) provide a basic overview.
The main points you should understand are:

* while a container could be considered as a virtual machine instance, the image that you find on the repositories are just the "starting condition" of the virtual drive of the container. Thus, every container could be extended after it has been created like any other system. Please consider that this is not considered a best practice, instead you should build a new image and run that... but for testing purposes could be considered ok as long as is well documented.
* As first operation every container need to be created and executed (`docker run` command), after you used it you can choose to destroy it (`--rm` flag) to start every time with a fresh an clean environment or too keep it and reuse using the commands `docker stop` and `docker start`.

Overview of some useful docker commands:

* **docker run**: pull the specified image, create the container and start it
* **docker exec**: run a single command inside the specified container
* **docker attach**: connect to a terminal inside the container (then detach with `ctrl+p ctrl+q`)
* **docker start**: start a container (that have been already created)
* **docker stop**: stop a running container
* **docker commit**: make the modification done to a container persistent in a new image.

Check the documentation pages here: [https://docs.docker.com/](https://docs.docker.com/) or this quick cheat sheet with the most useful commands here: [Docker Cheat sheet](https://github.com/ChristianLempa/cheat-sheets/blob/main/docker/docker.md)

# Using the workstation
Please remember that this is a shared workstation so to use it in an efficient way you should not run directly your heavy GPU computation, instead you should use the installed queue system that is described below. It allows a fair use of the shared resources avoiding behaviours that can lock the executions of the other users.

The docker flag `-it` is the combination of two flags: `-i` and `-t`. When you use the queue with docker you should not use the `-i` flag (it keeps your container running after the end) while you can use the `-t` to pipe the output of your script to the log file.

## Queue System
To exploit at most the use of GPUs in our lab and to help avoid overlaps among different experiments a new simple queue system has been developed and installed on the workstations.
In this way, instead of executing your script interactively you should submit your script to the queue and, as soon as the previous jobs in the queue are finished, it will be executed (even if you are not connected).

This system is based on [PBS Professional](https://www.altair.com/pbs-professional/) with some adaptations to be used in a single node scenario.

### How to use the queue
The first thing to do to use this system is that your experiments need to be run from a simple script able to perform everything you need without user interaction. This because you will submit this script to the queue and the system will run it whenever the resources are available (in this 1st version, this means when the GPU is available).

Let us assume that you script could be run in the following way: (input arguments are obviously optional)

    bash myRunner.sh inArg1 inArg2
The submission of this script to the queue can be done with the following command:

    rs_qsub.sh myjob 1:00:00 bash myRunner.sh inArg1 inArg2
where:

*  `rs_qsub.sh` is the command to submit the job to the queue
*  `myjob` Name of the job that will be shown in the queue (please include your name)
*  `1:00:00` Walltime, max execution time before killing the job (hhhh:mm:ss)
*  `bash` is the interpreter of your script
*  `myRunner.sh inArg...` is your script to be run with its arguments

**WARNING: due to a current docker limitation the queue cannot stop a running docker container, so if you use docker consider an adeguate walltime to avoid to trigger the stop procedure but at the same time not too long so other users can have a good estimation of the resource available**

Consider that the system will execute your script from the path where you submit the `rs_qsub` command, in the same folder there will be a file named `logXXX.o` (XXX is the ID of the job) containing both `STDOUT` and `STDERR` of the execution.

To create an interactive session use:

    rs_qinter.sh 6:00:00
where:

*  `rs_qinter.sh` is the command to reserve an interactive session on the queue
*  `6:00:00` Walltime, duration of the interactive session (h:mm:ss) or just the hours number (h)

These scripts will print on the console the id of your job, remember this code because it could be useful to manage the job in the next steps.

To monitor the status of the jobs in the queue:

    rs_qstat.sh
If nothing is printed means that no job are present in the queue, and thus your job already finished.

To delete a job from the queue use:
    
    rs_qdel.sh XXX
Where `XXX` is the ID of the job. The same command could be used to terminate an interactive session.

**WARNING: rs_qdel command does not work if your script is calling docker commands, due to a current docker limitation (still you can kill the job while is waiting in the queue)**

If you have any problem with jobs that need to killed or to extend the walltime write to the email address in the first paragraph. Please be careful and do not leave running jobs that use the GPU resources.
Notice that **GPU jobs that run for a long time without being in the queue will be killed.**

#### Test the queue
To check if the queue system is running correctly run a simple dummy script and check that the log file is created.
For example, inside `testscript.sh` write:

    #!/bin/bash
    echo "test script start"
    sleep 10
    echo "- - end"

run it with:

    rs_qsub.sh testJob 1:00:00 bash testscript.sh 

and check that a `XXX.pbs.log` file is created in the current folder

# Best Practice

The best solution is to write your code locally and then synchronize it to the workstation where he queue system eventually will execute it. In this way you don't need to deal with a remote GUI and possible slowdowns due to the execution of other users works.

## Sync your code and files to the workstation
In order to upload the code and files needed to perform your experiments you have multiple solutions. The best one is:

* keep your code in a git repository (i.e. hosted on Bitbucket). You can edit the code on your pc and the sync it to the workstation through the push/pull commands. Git act as versioning tool too so you are sure to not to loose track of any version of your code
* since git is not suited for large file transfer on all the workstation have been installed [RClone](https://rclone.org/) that can upload and download from any cloud storage service.

As a simpler alternative you can use any S-FTP client like the one integrated on mobaXterm or WinSCP to transfer all your files.

## Graphical User Interface and code sections
The execution of your code in the queue system is not structured to allow an easy use of a GUI or to require user interaction. 
The best advice is to use the queue just for the computation intensive tasks then saving the result to disk (if you use python [pickle](https://www.jessicayung.com/how-to-use-pickle-to-save-and-load-variables-in-python/) is your friend but many more advanced solution are also available) and finally generate all the graphs and result plots offline. 
Following the same concept, is a good practice to separate the parts of code to be run on the GPU (thus using the queue) from all that preprocessing steps and data preparation that do not need the GPU and moreover do not need to be run each time. 

## Workflow with Docker and the Queue System
Here after some guidelines on how to use the Queue system together with a docker container:

Please name your containers with the docker run flag `--name`, including your name so to easily identify the owner of the container and be careful to do not leave around container that you don't use and not to stop/delete containers that are not yours!

Unnamed or default named containers could be deleted in case of scarce resources.

**Warning! Don't use the -it docker flag in the queue jobs. Replace it with -t.**

### **Case 1:** When a ready to use image is available
In the case that you can find a pre-built container image that suits your dependency (i.e. check [this repository]( https://ngc.nvidia.com/catalog/containers/) that contains containers optimized for Nvidia GPUs) the easiest solution is a simple script that start up the container and runs your code in there. Then you could automatically clean up the container including the `--rm` flag.

### **Case 2:** OBSOLETE <s> When you need to customize your docker image </s>

<s>When you need more dependencies than the ones you can find in a pre-built image you can create your own.
Building a new derived image is considered to be the best practice since allows a great reproducibility and portability of your work, but being a more complex process you could opt for:

1.  create the container from one of the available image (usually the one that have at least the GPU SDK in it) don't use the --rm flag
    
    ```
    docker run -it [...]
    ```

2.  manually install your dependency

3.  detach and then stop the container

    ```
    docker stop [...]
    ```

4. commit to make the modification persistent in a new image
    
    ```
    docker commit [...]
    ```
    
5. remove the container

    ```
    docker rm [...]
    ```

Then inside your script that will be run by the queue system use a simple run command

```
docker run -t --rm [...]
```

</s>

### **Case 3:** When you need to customize your docker image, doing it the right way with docker files
The best way to create your own personalized docker image is to write a docker file that include a starting image and all the instruction to install your desired dependency in it.

More info [here](https://docs.docker.com/engine/reference/commandline/image_build/) and [here](https://docs.docker.com/get-started/).

Here after and example of docker file to build an image with GDAL, python and a set of python libraries defined in the file `requirement.txt`.

```
  FROM osgeo/gdal

  RUN apt-get update
  RUN apt install -y python3-pip
  RUN pip3 install --upgrade pip
  RUN pip3 install --upgrade setuptools

  COPY requirements.txt ./
  RUN pip3 install -r requirements.txt
```

## **Case 3.1:** Correct creation of a Docker container on a system with LDAP login system

When you use docker on a system with LDAP autentication system (all the RSLab GPU workstations and all the linux pc in the sensing lab) and you need to mount volumes from the host system, is important to configure the container to use the correct user and gorup id.

The procedure to achive this configuration is the following:
* duirng the build of the container create user and group with same ids as the host system (best way to do this is by Dockerfile arguments)
* run the container without mounting passwd and others, simply using -u and -g flags

Examples:
DockerFile
```
FROM nvcr.io/nvidia/pytorch:xxxx
ARG USERID=1000
ARG GROUPID=1000
RUN groupadd -g $GROUPID cg
RUN useradd -ms /bin/bash -u $USERID -g $GROUPID containeruser
COPY requirements.txt .
RUN pip install -r requirements.txt
```

docker build script
```
docker build --build-arg="USERID=$(id -u)" --build-arg="GROUPID=$(id -g)" -t yourname/exampletag .
```

docker run script
```
docker run [...] -u $(id -u):$(id -g) -h container -v /home/$USER:/home/containeruser -w /home/containeruser [...] yourname/exampletag [...]
```

## Cleanup the system when the project end

When your project end please clean up the files and folder created on the workstation, organize all the useful material in a zip archive and deliver it to your tutor (unless agreed differently). Please notilfy to the email in the 1st paragraph too that you are not using the workstation anymore.

## Remote Connection using Visual Studio Code
Here after the step-by-step guide to remotely connect to a workstation through [Visual Studio Code](https://code.visualstudio.com/). Firstly, be sure to be connected from within the university network or through a vpn (the University of Trento suggests using [GlobalProtect](https://wiki.unitn.it/pub:conf-vpn-paloalto)). 

From Visual Studio Code, install the extension [Remote - SSH](https://code.visualstudio.com/docs/remote/ssh). After the installation is complete, from the command palette (press <kbd>F1</kbd>, or the key combination <kbd>Ctrl+Shift+P</kbd>) select <b>Remote-SSH: Open SSH Configuration File...</b> and the path of your config file (usually **C:/Users/\<username\>/.ssh/config**). In the config file insert the address of the workstation you want to reach and save the edited file.

```
Host Alias1
    HostName ipaddress
    User username

Host Alias2
    HostName ipaddress
    User username
```

Now select from the command palette <b>Remote-SSH: Connect to Host...</b> and select the host you want to reach. Follow the prompt and insert the password when required. After connecting to the host, use the explorer to navigate inside your directory.

### **Connect without password prompt** 
If you want to connect without prompting your password each time you connect, you can store a private key on the workstation to grant you access. To do so, you need an ssh key that needs to match the key stored on the workstation. In Windows and Linux you can generate a key using the OpenSSH Client.

1. Open the command prompt as administrator and type  `ssh-keygen` (if it's the first time you do this, press enter avery time, using all the default options)
2. Navigate to the generated .ssh folder (usually **C:/Users/\<username\>/.ssh**)
3. Copy the **id_rsa.pub** key (open it with an editor, *id_rsa* is the default name of a ssh key, if you used a different name then copy the content of the corresponding .pub file)
4. Remote connect to the host set up in the previous passage and open your user directory (you will be required the password for this step)
5. In the .ssh folder of the workstation (i.e., **/home/\<username\>/.ssh**), paste the **id_rsa.pub** key in a new row inside **authorized_keys**

**NOTE**: If you use different SSH-keys for different servers (or a key name different from the default one *id_rsa*), you will need to specify the private key to use in the SSH command (i.e., with the option '`-i identity_file`'). If not specified, the default key will be used (i.e., **id_rsa**). To automate this, you can specify the ssh key to use for each host in the config file as follows (assuming that keys are stored in the default folder **C:/Users/\<username\>/.ssh**):

```
Host Alias1
    HostName ipaddress
    User username
    IdentityFile C:/Users/<username>/.ssh/id_rsa

Host Alias2
    HostName ipaddress
    User username
    IdentityFile C:/Users/<username>/.ssh/id_other_server_key_name
```

## Using hostnames in place of explicit IP addresses (Windows only, Linux case is WIP)
If you want to use an easy-to-remember name of your workstation instead of copy-pasting the IP address everywhere, you can do it by adding an entry to the windows host file.

**NOTE**: the **'ssh_config'** file works only with SSH connections using Open-SSH (e.g., Visual Studio Code), which are the most common scenario. However, in some cases where ssh (or Open-SSH) is not involved, it's useful to have a hostname for the workstation, which can be used in the browser or with certain apps (e.g., FileZilla uses PuTTY instead of Open-SSH). Follow these steps to add a hostname for your workstation:

1. Open this [link](https://rslab.disi.unitn.it/logger/host.php) and copy to your clipboard the line(s) associated to your workstation(s);
2. Open Notepad with administrator priviligies (right click > **Run as administrator**);
3. Click **File > Open >** Navigate to *C:\Windows\System32\drivers\etc*, select **All files (\*.\*)** in the botton-right box, select the **'hosts'** file and open it;
4. Paste the line from step 1 at the end of the file (or change the IP if you are updating the **'hosts'** file);
5. Save & Exit.

You are done! Now you can type the hostname in place of the IP address anywhere on your PC!

## Transfer files from and to the workstation with Filezilla and SSH-keys
You can exchange files with your workstation using FTP over SSH (i.e., SFTP), thus you can benefit from the previously set up SSH keys. However, FileZilla uses PuTTY for SSH, hence we need to tell FileZilla where the generate privates keys are:

1. Open FileZilla;
2. Click **Edit > Settings**;
3. Select the **SFTP** page (under **Connection**);
4. Click **Add key file** and navigate to your ssh keys folder (usually **C:/Users/\<username\>/.ssh**), and select your **private** key (**NOT** the one with *.pub* extensions, that is the public key! For example: **C:/Users/\<username\>/.ssh/id_rsa**);

Now this key will be used when an SSH key is required. To use the key you need to modify the Site configuration in FileZilla:

1. Open FileZilla;
2. Click **File > Open Site Manager**;
3. Select the Site to modify or create a new Site;
4. Under **Protocol**, select *SFTP*;
5. Under **Logon Type**, select either *Interactive* or *Key file*. In the first case it will use the keys saved previously by default, while in the second case a field named **Key file** will appear, and you will need to give the path to the corresponding SSH private key;
6. Under **Host**, insert the IP of the workstation (or the hostname defined in the *hosts* Windows file in case you modified the *hosts* file, see previous Section)
7. Press **Connect**, the first time it might ask for confirmation.

# Special applications

## QGis in a docker container

```
#!/bin/bash
docker run --rm -it -d --net host --name qgis -v $(pwd):$(pwd) qgis/qgis:release-3_28 bash
docker exec -u root qgis groupadd -g $(id -g) cg
docker exec -u root qgis useradd -ms /bin/bash -u $(id -u) -g $(id -g) containeruser
docker exec -u containeruser qgis cp -r /home/$USER/.Xauthority /home/containeruser/.Xauthority # For X11 to work, not necessary if not using remote access with X11-forwarding
docker exec -it -u containeruser -e DISPLAY=$DISPLAY -w $(pwd) qgis qgis
docker stop qgis
```

# Useful resources:

*  [General linux CLI commands and software]( https://linuxize.com/)
*  [Nvidia optimized Docker containers (useful for GPU accelerated DL applications)]( https://ngc.nvidia.com/catalog/containers/)
