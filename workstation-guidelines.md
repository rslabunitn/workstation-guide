# RSLAB Workstation Environment Guidelines

---

The RSLAB workstation environment is a shared workspace designed for PhD students/candidates, post-doctoral researchers, and Bachelor/Master students. It allows running computationally expensive code, especially those requiring GPU utilization. To maintain fair usage and optimize resource distribution, a set of guidelines is given.

These Guidelines provide an overview on the utilization of the shared environment. For more technical details, please refer to the RSLab `technical-guide.md` [technical guide](https://bitbucket.org/rslabunitn/workstation-guide/src/master/technical-guide.md).

## Guidelines Overview

---

To ensure fair access and resource utilization, the environment is divided into three main GPU groups:

* **Lithium:** Low-level GPUs (e.g., `rslab-itpar1`, `rslab-itpar2`, `rslab-y4`).
* **Magnesium:** Mid-level GPUs (e.g., `rslab-y5`, `rslab-y6`, `rslab-y7`).
* **Hydrogen:** High-level GPUs (e.g., `rslab-y1`, `rslab-y2`, `rslab-y3`).

## User Responsibilities

---

### Code and Architecture Optimization

- Optimize code, memory, and architecture to use only necessary resources.
- Consider the performance of individual machines; non-optimized code may perform worse on certain machines.

### Queue System

Two types of queues are available:

1. **Standard Queue (rs_qsub.sh):**
    - Execute bash scripts, defining walltime and script execution.
    - Book machines for the specified time, releasing them upon script completion.

2. **Interactive Queue (rs_qinter.sh):**
    - Use GPUs interactively for debugging or code development (e.g. Jupyter Notebooks / VSCode Debug).
    - Limit: 1 interactive job per user (across all workstation), walltime less than 8 hours (if greater, will be rounded to 8).

See the [technical guide](https://bitbucket.org/rslabunitn/workstation-guide/src/master/technical-guide.md) for the detailed syntax.

   **Note:** Standard Jobs with sleep commands and idle processing without using interactive queues are not allowed and could be terminated. Moreover, GPU can be used only within a standard or interactive Queue session (aka GPU used = job in the queue).

## File and Data Management

---

### Storage Locations

- **/home/user/:** Store scripts, code, and repositories.
- **/media/datapart/:** Ideal for large datasets and static files.
- **/media/faststorage/ (if available):** Use for faster data processing with SSD throughput.

Always keep a backup of the code by copying it on your local machine via scp/sfpt or with RClone on G-Drive (better) or with git (best). Remember that HDD and SSD can fail.

**Note:** The laboratory has a [Private Repository](https://bitbucket.org/rslabunitn/workspace/projects/RRR) where your source code can be stored. It's highly recommended to organize your source code and repositories using Git. 

### Data Handling

- Keep large datasets on */media/datapart/* or on */media/faststorage/*.
- In the absence of fast storage, if faster processing is required, copy data to */tmp/* or in a temporary folder in */home/user/* in the bash script and remove them after processing.
- Check and manage shared space on GPUs using `df -h` (entire drive) and `du -sh ./*` (local folder).
- When your project end please clean up the files and folders created on the workstation, organize all the useful material in a .zip archive and deliver it to your tutor (unless agreed differently). Please send a notification to the email in the main page specifyig that you are not using the workstation anymore.

### Docker Image Management

- Monitor and manage Docker images using `docker system df -v`.
- Regularly check and update or remove older images to prevent space issues.
- Tag your custom images with a meaningful name so to be easier to find it's owner.
- Unused images may be deleted by the system administrator in case of limited space.

**Note:** Keep Dockerfiles for rebuilding, as they consume less space.

